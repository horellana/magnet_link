require 'magnet_link'
require 'minitest/autorun'

class TestMagnetLink < Minitest::Test
  def setup
    @link = 'magnet:?xt=urn:btih:hexencodedsha-1oftorrentfile&dn=foo-dn&tr=foo-tr1&tr=foo-tr2&kt=music+mp3'
  end

  def test_xt
    url = 'urn:btih:hexencodedsha-1oftorrentfile'
    found = MagnetLink.new(@link).xt

    assert_equal 1, found.length
    assert_equal url, found.first
  end

  def test_dn
    dn = 'foo-dn'
    found = MagnetLink.new(@link).dn

    assert_equal 1, found.length
    assert_equal dn, found.first
  end

  def test_tr
    found = MagnetLink.new(@link).tr

    assert_equal 2, found.length
    assert_equal 'foo-tr1', found[0]
    assert_equal 'foo-tr2', found[1]
  end

  def test_kt
    found = MagnetLink.new(@link).kt

    assert_equal 2, found.length
    assert_equal %w(music mp3), found
  end

  def test_exception_is_raised
    assert_raises(MagnetLink::InvalidLink) do
      MagnetLink.new('invalid_link')
    end
  end
end
