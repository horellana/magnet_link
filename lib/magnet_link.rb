class MagnetLink
  def initialize(link)
    @parsed = parse_link link
    @parameters = [:xt, :tr, :dn, :kt]
  end

  def method_missing(method, *args, &block)
    super unless @parameters.include? method
    find_values(method)
  end

  def respond_to_missing?(method)
    @parameters.include? method
  end

  private

  def find_values(parameter)
    @parsed[parameter]
  end

  def normalize_value(parameter, value)
    return value.split('+') if parameter == :kt
    return value
  end

  def parse_link(link)
    parsed = {}

    link.split('&').each do |chunk|
      match = chunk.match('(.{2})=(.+)')

      if match.nil?
        raise InvalidLink
      end

      parameter = match.captures[0].to_sym
      value = match.captures[1]

      parsed[parameter] = [] unless parsed[parameter]
      parsed[parameter].push normalize_value(parameter, value)
    end

    parsed.each_key { |key| parsed[key].flatten! }
  end

  class InvalidLink < RuntimeError
  end
end
