Gem::Specification.new do |s|
  s.name        = 'magnet_link'
  s.version     = '0.0.9'
  s.date        = '2017-03-25'
  s.summary     = 'magnet_link'
  s.description = 'magnet links parser'
  s.authors     = ['Hector Orellana']
  s.email       = 'hofm92@gmail.com'
  s.files       = ['lib/magnet_link.rb']
  s.homepage    =
    'https://gitlab.com/juiko/magnet_link'
  s.license       = 'GPL-3.0'
end
